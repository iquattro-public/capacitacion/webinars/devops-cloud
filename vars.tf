variable "do_token" {
  type        = string
  description = "Digitalocan API Token"
}

variable "ssh_id" {
  type        = string
  description = "SSH Public Key"
}

variable "ssh_key" {
  type        = string
  description = "SSH Public Key"
}

variable "droplet_image" {
  type        = string
  default     = "145090142"
  description = " Digitalocean Image ID, Fedora 39 "
}

variable "droplet_region" {
  type        = string
  default     = "nyc3"
  description = "Use Droplet region lon1"
}

variable "droplet_size" {
  type        = string
  default     = "s-1vcpu-1gb"
  description = "Digitalocean ID Droplet size"
}

variable "droplet_name" {
  type        = string
  default     = "iquattro-srv"
  description = "Name for the new droplet"
}

variable "do_tag" {
  type        = string
  default     = "demo"
  description = "Digitalocean Tags Econotec"
}
