## Data template
data "template_file" "init" {
  template = file("templates/cloud-init.yml")
  vars = {
    init_ssh_public_key_devops = "${var.ssh_key}"
  }
}

## Digitalocean Tags
resource "digitalocean_tag" "droplet_tag" {
  name = var.do_tag
}

## Data Digitalocean VPC
data "digitalocean_vpc" "vpc" {
  region = var.droplet_region
}

## Create a new Droplet
resource "digitalocean_droplet" "droplet" {
  name       = var.droplet_name
  image      = var.droplet_image
  region     = var.droplet_region
  size       = var.droplet_size
  monitoring = true
  vpc_uuid   = data.digitalocean_vpc.vpc.id
  ssh_keys   = [var.ssh_id]
  tags       = [digitalocean_tag.droplet_tag.name]
  user_data  = data.template_file.init.rendered
}
